<?php

/*
Plugin Name: Axalys booking
Plugin URI: 
Description: Autobypark - Réservations parking
Version: 1.0
Author: Axalys Technologies
Author URI: http://axalys.com
*/


/**
 * Load library for admin
 */
function axalys_booking_admin_scripts() {
        

		wp_enqueue_style( 'bootstrapcss','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, null );
		wp_enqueue_style( 'my-jquery-ui-datepicker',plugin_dir_url( __FILE__ ) . 'css/datepicker.css');
        wp_enqueue_style( 'main_css', plugin_dir_url( __FILE__ ) . 'css/style.css' );

        wp_enqueue_script( 'jquery_default', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), '3.1.1', true ); 
        wp_enqueue_script( 'bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
		wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_enqueue_script('admin_script',  plugin_dir_url( __FILE__ ) . 'js/admin.js', array('jquery_default'), '1.0');

        wp_enqueue_media();

}

add_action('admin_enqueue_scripts', 'axalys_booking_admin_scripts');


/**
 * Load library for front
 */
function axalys_booking_scripts() {

    wp_enqueue_style( 'bootstrapcss','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, null );
    wp_enqueue_style( 'my-jquery-ui-datepicker',plugin_dir_url( __FILE__ ) . 'css/datepicker.css');

    wp_enqueue_script( 'jquery_default', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), '3.1.1', true );
    wp_enqueue_script('script_axalys_booking',  plugin_dir_url( __FILE__ ) . 'js/script_booking.js', array('jquery_default'), '1.0');
	wp_enqueue_script( 'jquery-ui-datepicker' );

	wp_enqueue_script('script_axalys_bootstrap_validator', 'https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.js', array(), '0.11.9', 'all');

	wp_localize_script('script_axalys_booking', 'ajaxurl', admin_url( 'admin-ajax.php' ) );                         

}

add_action('wp_enqueue_scripts', 'axalys_booking_scripts');

/**
 * Création du custom post
 */
function axa_booking_custom_post_type() {

	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Réservations', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Réservation', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Réservations'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les réservations'),
		'view_item'           => __( 'Voir les réservation'),
		'add_new_item'        => __( 'Ajouter une nouvelle réservation'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer la réservation'),
		'update_item'         => __( 'Modifier la réservation'),
		'search_items'        => __( 'Rechercher une réservation'),
		'not_found'           => __( 'Non trouvée'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);

	$args = array(
		'label'               => __( 'Réservations'),
		'description'         => __( 'Liste des réservations'),
		'labels'              => $labels,
		// On définit les options disponibles dans l'éditeur de notre custom post type ( un titre, un auteur...)
		'supports'            => array( 'title', 'author', 'revisions'),
		/* 
		* Différentes options supplémentaires
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'axabooking'),

	);

	register_post_type( 'axabooking', $args );

}

add_action( 'init', 'axa_booking_custom_post_type', 0 );

/**
 * @param $param
 * @return string
 * Permet d'ajouter ou non le champ check a un input checkbox
 */
function verif_check_box($param) {
    if ( $param == 'true' )
        return 'checked';
    else
        return '';
}


/**
 *
 */
function axa_booking_infos_box() {
    add_meta_box( 
        'axa-infos',
        __( 'Informations', 'myplugin_textdomain' ),
        'axa_booking_infos_box_content',
        'axabooking',
        'normal',
        'high'
    );
}

add_action( 'add_meta_boxes', 'axa_booking_infos_box' );

/**
 * @param $post
 */
function axa_booking_infos_box_content( $post ) {

	global $post;
 	
 	wp_nonce_field( basename( __FILE__ ), 'infos_fields' );

 	$surname = get_post_meta($post->ID, 'axa-sur-name', true);
 	$firstname = get_post_meta($post->ID, 'axa-first-name', true);
 	$company = get_post_meta($post->ID, 'axa-company', true);
 	$address = get_post_meta($post->ID, 'axa-address', true);
 	$postalcode = get_post_meta($post->ID, 'axa-postal-code', true);
 	$city = get_post_meta($post->ID, 'axa-city', true);
 	$mail = get_post_meta($post->ID, 'axa-mail', true);
 	$phone = get_post_meta($post->ID, 'axa-portable', true);
	echo '
			<div class="form-group row">
				<label for="axa-sur-name" class="col-sm-2 col-form-label">Nom</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-sur-name" name="axa-sur-name" placeholder="Nom de famille" value="' . esc_textarea( $surname )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-first-name" class="col-sm-2 col-form-label">Prénom</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-first-name" name="axa-first-name" placeholder="Prénom" value="' . esc_textarea( $firstname )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-company" class="col-sm-2 col-form-label">Société</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-company" name="axa-company" placeholder="Société" value="' . esc_textarea( $company )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-address" class="col-sm-2 col-form-label">Adresse</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-address" name="axa-address" placeholder="Adresse" value="' . esc_textarea( $address )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-postal-code" class="col-sm-2 col-form-label">Code postal</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-postal-code" name="axa-postal-code" placeholder="Code postal" value="' . esc_textarea( $postalcode )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-city" class="col-sm-2 col-form-label">Ville</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-city" name="axa-city" placeholder="Ville" value="' . esc_textarea( $city )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-mail" class="col-sm-2 col-form-label">E-mail</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-mail" name="axa-mail" placeholder="utilisateur@domaine.com" value="' . esc_textarea( $mail )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-portable" class="col-sm-2 col-form-label">Portable</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-portable" name="axa-portable" placeholder="Portable" value="' . esc_textarea( $phone )  . '">
				</div>
			</div>
	';
}

/**
 *
 */
function axa_booking_infos_car_box() {
    add_meta_box( 
        'axa-infos-car',
        __( 'Véhicule', 'myplugin_textdomain' ),
        'axa_booking_infos_car_box_content',
        'axabooking',
        'normal',
        'high'
    );
}

add_action( 'add_meta_boxes', 'axa_booking_infos_car_box' );


/**
 * @param $post
 */
function axa_booking_infos_car_box_content( $post ) {
 	//wp_nonce_field( plugin_basename( __FILE__ ), 'product_price_box_content_nonce' );

 	global $post;
 	wp_nonce_field( basename( __FILE__ ), 'infos_fields' );
 	$car_mark = get_post_meta($post->ID, 'axa-car-mark', true);
 	$car_model = get_post_meta($post->ID, 'axa-car-model', true);
 	$car_immatriculation = get_post_meta($post->ID, 'axa-car-immatriculation', true);
	echo '
			<div class="form-group row">
				<label for="axa-car-mark" class="col-sm-2 col-form-label">Marque</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-car-mark" name="axa-car-mark" placeholder="Nom" value="' . esc_textarea( $car_mark )  . '">
				</div>
				<label for="axa-car-model" class="col-sm-2 col-form-label">Modèle</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-car-model" name="axa-car-model" placeholder="Modèle" value="' . esc_textarea( $car_model )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-car-immatriculation" class="col-sm-2 col-form-label">Immatriculation</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="axa-car-immatriculation" name="axa-car-immatriculation" placeholder="Immatriculation" value="' . esc_textarea( $car_immatriculation )  . '">
				</div>
			</div>
	';
}

/**
 *
 */
function axa_booking_infos_fly_box() {
    add_meta_box(
      'axa-infos-fly',
      __('Vols', 'myplugin_textdomain'),
      'axa_booking_infos_fly_box_content',
      'axabooking',
      'normal',
      'high'
    );
}

add_action( 'add_meta_boxes', 'axa_booking_infos_fly_box' );


/**
 * @param $post
 */
function axa_booking_infos_fly_box_content( $post ) {
 	//wp_nonce_field( plugin_basename( __FILE__ ), 'product_price_box_content_nonce' );

 	global $post;
 	$fly_date_depart = get_post_meta($post->ID, 'axa-fly-date-depart', true);
 	$fly_start = get_post_meta($post->ID, 'axa-fly-start', true);
 	$fly_desired = get_post_meta($post->ID, 'axa-desired', true);
 	$fly_date_return = get_post_meta($post->ID, 'axa-fly-date-return', true);
 	$fly_return = get_post_meta($post->ID, 'axa-fly-return', true);
 	$return_none = get_post_meta($post->ID, 'axa-return-none', true);
	echo '
			<div class="form-group row">
				<label for="axa-fly-date-depart" class="col-sm-2 col-form-label">Date de départ</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-fly-date-depart" name="axa-fly-date-depart" placeholder="01/01/1970" value="' . esc_textarea( $fly_date_depart )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-fly-start" class="col-sm-2 col-form-label">Heure de décollage</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-fly-start" name="axa-fly-start" placeholder="12h30" value="' . esc_textarea( $fly_start )  . '">
				</div>
				<label for="axa-desired" class="col-sm-2 col-form-label">Heure souhaitée</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-desired" name="axa-desired" placeholder="12h30" value="' . esc_textarea( $fly_desired )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-fly-date-return" class="col-sm-2 col-form-label">Date de retour</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-fly-date-return" name="axa-fly-date-return" placeholder="01/01/1970" value="' . esc_textarea( $fly_date_return )  . '">
				</div>
			</div>
			<div class="form-group row">
				<label for="axa-fly-return" class="col-sm-2 col-form-label">Heure de retour</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="axa-fly-return" name="axa-fly-return" placeholder="12h30" value="' . esc_textarea( $fly_return )  . '">
				</div>
				<label for="axa-desired" class="col-sm-2 col-form-label">Heure inconnue</label>
				<div class="col-sm-4">
						<input type="checkbox" class="form-check-input" id="axa-return-none" name="axa-return-none" ' . verif_check_box($return_none) . '>
				</div>
			</div>
	';
}

/**
 *
 */
function axa_booking_mail_box() {
    add_meta_box( 
        'axa-mail-box',
        __( 'Mail', 'myplugin_textdomain' ),
        'axa_booking_mail_box_content',
        'axabooking',
        'normal',
        'high'
    );
}

add_action( 'add_meta_boxes', 'axa_booking_mail_box' );

/**
 * @param $post
 */
function axa_booking_mail_box_content( $post ) {
 	//wp_nonce_field( plugin_basename( __FILE__ ), 'product_price_box_content_nonce' );

 	global $post;
 	$fly_date_depart = get_post_meta($post->ID, 'axa-fly-date-depart', true);
 	$fly_start = get_post_meta($post->ID, 'axa-fly-start', true);
 	$fly_desired = get_post_meta($post->ID, 'axa-desired', true);
 	$fly_date_return = get_post_meta($post->ID, 'axa-fly-date-return', true);
 	$fly_return = get_post_meta($post->ID, 'axa-fly-return', true);
	echo '
            <div class="alert alert-success hidden" id="send-mail">
                <strong>Information : </strong> Le mail a été envoyé.
            </div>
            
            <div class="alert alert-warning hidden" id="param-mail">
                <strong>Information : </strong> Attention à modifier les champs dans le mail.
            </div>
            
			<div class="from-group">
				<div class="col-sm-2">
					<label for="mail_subject">Sujet</label>
				</div>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="mail-subject" name="mail-subject" value="Sujet">
				</div>
			</div>
			<div class="from-group">
				<div class="col-sm-2">
					<label for="mail_subject">Message</label>
				</div>
				<div class="col-sm-10">
					<textarea class="form-control" id="area-mail" rows="6"></textarea>
				</div>
			</div>

			<button type="" class="btn btn-primary" id="btn-axa-validation">Validation</button>
			<button type="" class="btn btn-primary" id="btn-axa-modification">Modification</button>
			<button type="submit" class="btn btn-primary" id="btn-axa-send">Envoyer</button>
	';
}

function axa_booking_validate_box() {
    add_meta_box( 
        'axa-validate-box',
        __( 'Statut', 'myplugin_textdomain' ),
        'axa_booking_validate_box_content',
        'axabooking',
        'normal',
        'high'
    );
}

add_action( 'add_meta_boxes', 'axa_booking_validate_box' );

function axa_booking_validate_box_content( $post ) {
	echo '
		<div class="form-group">
			<div class="col-sm-2">
				<label for="axa-statut-booking">Statut actuel</label>
			</div>
			<div class="col-sm-10">
				<select class="form-control" id="axa-statut-booking" name="axa-statut-booking">
					<option value="validate">Validé</option>
					<option value="waiting">En attente</option>
					<option value="canceled">Annulé</option>
				</select>
			</div>
		</div>
	';
}

/**
 * Save the metabox data
 */
function wpt_save_events_meta( $post_id, $post ) {
	
// Return if the user doesn't have edit permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}
	// Verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times.
	//write_log(wp_verify_nonce( $_POST['infos_fields'], basename(__FILE__) ));
	if ( 
		! isset( $_POST['axa-first-name'] )
		|| ! isset( $_POST['axa-sur-name'] )
		|| ! wp_verify_nonce( $_POST['infos_fields'], basename(__FILE__) ) 
	) {
		
		return $post_id;
	}

	// Now that we're authenticated, time to save the data.
	// This sanitizes the data from the field and saves it into an array $events_meta.
	$meta['axa-sur-name'] = sanitize_text_field( $_POST['axa-sur-name'] );
	$meta['axa-first-name'] = sanitize_text_field( $_POST['axa-first-name'] );
	$meta['axa-company'] = sanitize_text_field( $_POST['axa-company'] );
	$meta['axa-address'] = sanitize_text_field( $_POST['axa-address'] );
	$meta['axa-city'] = sanitize_text_field( $_POST['axa-city'] );
	$meta['axa-postal-code'] = sanitize_text_field( $_POST['axa-postal-code'] );
	$meta['axa-mail'] = sanitize_text_field( $_POST['axa-mail'] );
	$meta['axa-portable'] = sanitize_text_field( $_POST['axa-portable'] );
	$meta['axa-car-mark'] = sanitize_text_field( $_POST['axa-car-mark'] );
	$meta['axa-car-model'] = sanitize_text_field( $_POST['axa-car-model'] );
	$meta['axa-car-immatriculation'] = sanitize_text_field( $_POST['axa-car-immatriculation'] );
	$meta['axa-fly-date-depart'] = sanitize_text_field( $_POST['axa-fly-date-depart'] );
	$meta['axa-fly-start'] = sanitize_text_field( $_POST['axa-fly-start'] );
	$meta['axa-desired'] = sanitize_text_field( $_POST['axa-desired'] );
	$meta['axa-fly-date-return'] = sanitize_text_field( $_POST['axa-fly-date-return'] );
	$meta['axa-fly-return'] = sanitize_text_field( $_POST['axa-fly-return'] );
	$meta['axa-statut-booking'] = sanitize_text_field( $_POST['axa-statut-booking']);
	if ( isset($_POST['axa-return-none']) )
		$meta['axa-return-none'] = 'true';
	else
		$meta['axa-return-none'] = 'false';


	// Cycle through the $events_meta array.
	// Note, in this example we just have one item, but this is helpful if you have multiple.
	foreach ( $meta as $key => $value ) :
		// Don't store custom data twice
		if ( 'revision' === $post->post_type ) {
			return;
		}
		if ( get_post_meta( $post_id, $key, false ) ) {
			// If the custom field already has a value, update it.
			update_post_meta( $post_id, $key, $value );
		} else {
			// If the custom field doesn't have a value, add it.
			add_post_meta( $post_id, $key, $value);
		}
		if ( ! $value ) {
			// Delete the meta key if there's no value
			delete_post_meta( $post_id, $key );
		}

	endforeach;

}
add_action( 'save_post', 'wpt_save_events_meta', 1, 2 );


add_action( 'wp_ajax_booking_form', 'booking_form' );
add_action( 'wp_ajax_nopriv_booking_form', 'booking_form' );

/**
 *
 */
function booking_form() {

    
    $content['firstname'] = sanitize_text_field($_POST['firstname']);
    $content['surname'] = sanitize_text_field($_POST['surname']);
    $content['company'] = sanitize_text_field($_POST['company']);
    $contant['address'] = sanitize_text_field($_POST['address']);
    $contant['city'] = sanitize_text_field($_POST['city']);
    $content['postal'] = sanitize_text_field($_POST['postal']);
	$content['mail'] = sanitize_text_field($_POST['mail']);
	$content['phone'] = sanitize_text_field($_POST['phone']);
	$content['car_mark'] = sanitize_text_field($_POST['car_mark']);
	$content['car_model'] = sanitize_text_field($_POST['car_model']);
	$content['car_immatriculation'] = sanitize_text_field($_POST['car_immatriculation']);
 	$content['fly_date_depart'] = sanitize_text_field($_POST['fly_date_depart']);
 	$content['fly_start'] = sanitize_text_field($_POST['fly_start']);
 	$content['hour_desired'] = sanitize_text_field($_POST['hour_desired']);
 	$content['fly_date_return'] = sanitize_text_field($_POST['fly_date_return']);
 	$content['fly_return'] = sanitize_text_field($_POST['fly_return']);
 	$content['return_none'] = sanitize_text_field($_POST['return_none']);

    // Making post
    $post_arr = array(
	    'post_title'   => 'Booking from ' . $firstname,
	    'post_status'  => 'publish',
	    'post_author'  => get_current_user_id(),
	    'post_type'	   => 'axabooking',
	    'meta_input'   => array(
	        'axa-first-name' => $content['firstname'],
	        'axa-sur-name' => $content['surname'],
	        'axa-company' => $content['company'],
	        'axa-address' => $content['address'],
	        'axa-city' => $content['city'],
	        'axa-postal-code' => $content['postal'],
	        'axa-mail' => $content['mail'],
	        'axa-portable' => $content['phone'],
	        'axa-car-mark' => $content['car_mark'],
	        'axa-car-model' => $content['car_model'],
	        'axa-car-immatriculation' => $content['car_immatriculation'],
	        'axa-fly-date-depart' => $content['fly_date_depart'],
	        'axa-fly-start' => $content['fly_start'],
	        'axa-desired' => $content['hour_desired'],
	        'axa-fly-date-return' => $content['fly_date_return'],
	        'axa-fly-return' => $content['fly_return'],
	        'axa-return-none' => $content['return_none']
	    ),
	);

	wp_insert_post( $post_arr );

	// Send to ajax 
    echo json_encode($content);
    die();
}

// Add the custom columns to the book post type:
add_filter( 'manage_axabooking_posts_columns', 'set_custom_edit_booking_columns' );

/**
 * @param $columns
 * @return mixed
 */
function set_custom_edit_booking_columns($columns) {
    
    unset( $columns['author'] );
    $columns['surname'] = __( 'Nom', 'your_text_domain' );
    $columns['firstname'] = __( 'Prénom', 'your_text_domain' );
    $columns['statut'] = __('Statut', 'your_text_domain');
    $columns['author'] = __( 'Auteur', 'your_text_domain' );

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_axabooking_posts_custom_column' , 'custom_booking_column', 100, 2 );

/**
 * @param $column
 * @param $post_id
 */
function custom_booking_column( $column, $post_id ) {
    switch ( $column ) {
        case 'surname' :
            echo get_post_meta( $post_id , 'axa-sur-name' , true ); 
            break;

        case 'firstname' :
            echo get_post_meta( $post_id , 'axa-first-name' , true ); 
            break;

        case 'statut' :
        	echo get_post_meta( $post_id, 'axa-statut-booking', true);
        	break;
    }
}

if ( ! function_exists('write_log')) {
    /**
     * @param $log
     */
    function write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
            error_log( print_r( $log, true ) );
        } else {
            error_log( $log );
        }
    }
}

add_action( 'wp_ajax_booking_mail', 'booking_mail' );
add_action( 'wp_ajax_nopriv_booking_mail', 'booking_mail' );

/**
 * Ajax - send mail from back office
 */
function booking_mail() {
    $array = 'success';
    echo json_encode($array);
    die();
}

function general_admin_notice(){
    global $pagenow;
    if ( $pagenow == 'options-general.php' ) {
         echo '<div class="notice notice-warning is-dismissible">
             <p>This notice appears on the settings page.</p>
         </div>';
    }
}
add_action('admin_notices', 'general_admin_notice');

require_once('page_templater.php');
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );