<?php 

get_header();
?>

<div class="container">
	<div class="alert alert-success alert-dismissable fade in hidden" id="save-success">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success : </strong> Bien envoyé !
	</div>

	<form id="booking-form" class="well" method="post" data-toggle="validator">
		<div class="jumbotron" style="height: 450px">
			<h2>Informations personnelles</h3>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="surname">Nom</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="text" name="surname" class="form-control" id="surname" placeholder="Nom" required>
					</div>
				</div>
				<div class="form-group col-md-6 has-feedback">
					<label for="firstname">Prénom</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="mail" name="firstname" class="form-control" id="firstname" placeholder="Prénom" required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="company">Société</label>
					<input type="text" name="company" class="form-control" id="company" placeholder="Société">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="address">Adresse</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
						<input type="text" name="address" class="form-control" id="address" placeholder="Adresse" required>
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="city">Ville</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
						<input type="text" name="city" class="form-control" id="city" placeholder="Ville" required>
					</div>
				</div>
				<div class="form-group col-md-6">
					<label for="postal">Code postal</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
						<input type="text" name="postal" class="form-control" id="postal" placeholder="Code postal" pattern="[0-9]{5}" data-error="Ce n'est pas un code postal." required>
					</div>
					<div class="help-block with-errors"></div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="mail">E-mail</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="email" name="mail" class="form-control is-invalid" id="mail" placeholder="utilisateur@domaine.com" >
					</div>
					<div class="help-block with-errors">
					</div>
				</div>
				<div class="form-group col-md-6">
					<label for="phone">Portable</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
						<input type="text" name="phone" class="form-control" id="phone" placeholder="Portable">
					</div>
				</div>
			</div>
		</div>
		<div class="jumbotron" style="height: 200px">
			<h2>Véhicule</h3>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="car-mark">Marque</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-dashboard"></i></span>
						<input type="text" name="car-mark" class="form-control" id="car-mark" placeholder="Marque">
					</div>
				</div>
				<div class="form-group col-md-6">
					<label for="car-model">Modèle</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-dashboard"></i></span>
						<input type="text" name="car-model" class="form-control" id="car-model" placeholder="Modèle">
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="car-immatriculation">Immatriculation</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-dashboard"></i></span>
						<input type="text" name="car-immatriculation" class="form-control" id="car-immatriculation" placeholder="Immatriculation">
					</div>
				</div>
			</div>
		</div>
		<div class="jumbotron" style="height: 330px">
			<h2>Voyage</h3>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="fly-date-depart">Date de départ</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-plane"></i></span>
						<input type="text" name="fly-date-depart" class="form-control" id="fly-date-depart" placeholder="01/01/1970">
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="fly-start">Heure de décollage</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
						<input type="text" name="fly-start" class="form-control" id="fly-start" placeholder="12h30">
					</div>
				</div>
				<div class="form-group col-md-6">
					<label for="hour-desired">Heure souhaitée</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
						<input type="text" name="hour-desired" class="form-control" id="hour-desired" placeholder="12h30">
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-12">
					<label for="fly-date-return">Date de retour</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-plane"></i></span>
						<input type="text" name="fly-date-return" class="form-control" id="fly-date-return" placeholder="01/01/1970">
					</div>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="fly-return">Heure de retour</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
						<input type="text" name="fly-return" class="form-control" id="fly-return" placeholder="12h30">
					</div>
				</div>
				<div class="form-group col-md-6">
					<label for="return-none">Heure inconnue</label>
						<input type="checkbox" name="return-none" class="form-check-input" id="return-none">
				</div>
			</div>
		</div>

		<button class="btn btn-primary" type="submit">Valider</button>
	</form>
</div>

<?php
get_footer();
?>