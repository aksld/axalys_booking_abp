$(function(){
    $('#booking-form').submit(function(e) {

        // Si le formulaire n'est pas validé en front
        if ( e.isDefaultPrevented()) {
            return;
        }

        // Stop propagation
        e.preventDefault();

        // Listing all inputs
        var firstname = $('#firstname').val();
        var surname = $('#surname').val();
        var company = $('#company').val();
        var address = $('#address').val();
        var city = $('#city').val();
        var postal = $('#postal').val();
        var mail = $('#mail').val();
        var phone = $('#phone').val();
        var car_mark = $('#car-mark').val();
        var car_model = $('#car-model').val();
        var car_immatriculation = $('#car-immatriculation').val();
        var fly_date_depart = $('#fly-date-depart').val();
        var fly_start = $('#fly-start').val();
        var hour_desired = $('#hour-desired').val();
        var fly_date_return = $('#fly-date-return').val();
        var fly_return = $('#fly-return').val();

        // Checkbox : heure de retour inconnue
        var return_none = false;
        if ( $('#return-none').is(':checked') )
            return_none = true;
        alert(return_none);
        // Starting ajax to save form
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                 'action': 'booking_form',
                 'firstname' : firstname,
                 'surname' : surname,
                 'company' : company,
                 'address' : address,
                 'city' : city,
                 'postal' : postal,
                 'mail' : mail,
                 'phone' : phone,
                 'car_mark' : car_mark,
                 'car_model' : car_model,
                 'car_immatriculation' : car_immatriculation,
                 'fly_date_depart' : fly_date_depart,
                 'fly_start' : fly_start,
                 'hour_desired' : hour_desired,
                 'fly_date_return' : fly_date_return,
                 'fly_return' : fly_return,
                 'return_none' : return_none
             },
            dataType: 'json',
            success: function(json) {
                if(json.isSuccess) 
                {
                    alert('success');
                    $('#save-success').removeClass('hidden');
                }
                else
                {
                    alert('error');
                }                
            }
        });
    });

    jQuery('#fly-date-depart').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    jQuery('#fly-date-return').datepicker({
        dateFormat: 'dd/mm/yy'
    });

    $("#return-none").change(function() {
        $("#fly-return").prop("readonly", this.checked);

    });

    $("#btn-axa-validation").click(function(e) {
        e.preventDefault();
        alert('clicked');
    });
})

